package com.lfds.javaweb.proyecto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.lfds.javaweb.proyecto.model.Register;
import com.lfds.javaweb.proyecto.repositories.RegisterRepository;
import com.lfds.javaweb.proyecto.service.RegisterService;

@SpringBootTest
class ProyectoMomentumApplicationTests {

	@Autowired
	private RegisterService registerService;

	@Autowired
	private RegisterRepository registerRepository;

	@BeforeEach
	@Test
	void fileExistanceUnitTest() {
		//setup
		boolean bol = false;
		//execute
		bol = registerService.uploadDatabase();
		//validate
		assertTrue(bol, "Couldn't find or upload file");
	}
	
	@Test
	void databaseHasContentUnitTest() {
		// setup
		List<Register> reg = null;
		// execute
		reg = (List<Register>) registerRepository.findAll();
		// validate
		assertFalse(reg.isEmpty(), "Registers empty");
	}

	@Test
	void expectedNumberofRegistersUnitTest() {
		// setup
		long expectedNumber = 1938;
		// execute
		long actualNumber = registerRepository.count();
		// validate
		assertEquals(expectedNumber, actualNumber, "Database registers Error");
	}

	@Test
	void firstEmployeeHasRegistersUnitTest() {
		// setup
		List<Register> reg = null;
		// execute
		reg = registerRepository.getByPeriodAndName("CXMG");
		// validate
		assertFalse(reg.isEmpty(), "Registers empty");
	}
	
	@Test
	void firstEmployeeIntegrationTest() {
		//setup
		String name = "CXMG";
		//execute
		List<Register> reg = registerService.getAll();
		//validate
		assertEquals(reg.get(0).getName(), name, "First employee doesn't match");
	}

}
