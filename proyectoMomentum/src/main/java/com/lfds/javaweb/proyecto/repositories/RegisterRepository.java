package com.lfds.javaweb.proyecto.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.lfds.javaweb.proyecto.model.Register;

public interface RegisterRepository extends CrudRepository<Register, Integer>{

	@Query("SELECT r FROM Register r WHERE r.date LIKE %?2% AND r.name = ?1")
	List<Register> getByMonthAndName(String name, String month);
	
	@Query("SELECT r FROM Register r WHERE r.date LIKE %?1%")
	List<Register> getByMonth(String month);
	
	@Query("SELECT r FROM Register r WHERE r.name LIKE %?1%")
	List<Register> getByPeriodAndName(String name);
}
