package com.lfds.javaweb.proyecto.service;

import java.util.List;

import com.lfds.javaweb.proyecto.model.Register;

public interface RegisterService {
	void save(Register r);
	float getByMonthAndName(String name, String month);
	List<Register> getByMonth(String month);
	List<Register> getByPeriodAndName(String is, String startDate, String endDate);
	List<Register> getAllByPeriod(String startDate, String endDate);
	List<Register> getAll();
	boolean uploadDatabase();
}
