package com.lfds.javaweb.proyecto.controller;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lfds.javaweb.proyecto.model.Check;
import com.lfds.javaweb.proyecto.model.Register;
import com.lfds.javaweb.proyecto.service.RegisterService;

@Controller
public class ViewsController {
	
	@Autowired
	RegisterService registerService;

    //1st EndPoint
	@PostMapping("/user_view")
    public String hoursPerMonthAndName(Model model, @RequestParam String is, @RequestParam String mes) {
    	if(registerService.getByMonthAndName(is,mes) == -1.0) {
    		return "error";
    	}
    	model.addAttribute("hours", registerService.getByMonthAndName(is, mes));
    	model.addAttribute("is", is);
    	LocalDate date = LocalDate.parse("2020-" + mes + "-12");
    	model.addAttribute("mes", date.getMonth().name().toLowerCase());
    	return "hours";
    }
    
	@GetMapping("/first_form")
	public String firstForm() {
		return "firstForm";
	}
	
	//2nd EndPoint
    @PostMapping("/users_view")
	public String registersPerPeriodAndName(Model model, @RequestParam String is, @RequestParam String startDate,
			@RequestParam String endDate) {
		if(registerService.getByPeriodAndName(is, startDate, endDate).isEmpty())
			return "error";
		model.addAttribute("list", registerService.getByPeriodAndName(is, startDate, endDate));
		return "periodMonth";
	}
    
    @GetMapping("/second_form")
	public String secondForm() {
		return "firstForm2";
	}
    
    //3rd EndPoint
    @PostMapping("/period_view")
    public String registerPerMonth(Model model, @RequestParam String mes) {
    	if(registerService.getByMonth(mes).isEmpty())
			return "error";	
    	model.addAttribute("list", registerService.getByMonth(mes));
    	return "periodMonth";
    }
    
    @GetMapping("/tird_form")
   	public String tirdForm() {
   		return "firstForm3";
   	}
    
    //4rt EndPoint
    @PostMapping("/periods")
	public String allRegisterPerPeriod(Model model, @RequestParam String startDate,
			@RequestParam String endDate){
		if(registerService.getAllByPeriod(startDate, endDate).isEmpty())
			return "error";
		model.addAttribute("list", registerService.getAllByPeriod(startDate, endDate));
		return "periodMonth";
	}
    
    @GetMapping("/fourth_form")
   	public String fourthForm() {
   		return "firstForm4";
   	}
    
    @GetMapping("/errorPage")
    public String error() {
    	return "error";
    }
    
    @GetMapping({"/", "/index"})
    public String index() {
    	registerService.uploadDatabase();
    	return "index";
    }
   
}