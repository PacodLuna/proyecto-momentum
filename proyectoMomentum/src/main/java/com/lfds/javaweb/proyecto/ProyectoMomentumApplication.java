package com.lfds.javaweb.proyecto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@EnableJpaRepositories("com.lfds.javaweb.proyecto.repositories") 
@EntityScan("com.lfds.javaweb.proyecto.model")
@SpringBootApplication
public class ProyectoMomentumApplication extends SpringBootServletInitializer {
    
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder app) {
      return app.sources(ProyectoMomentumApplication.class); 
    }
    
	public static void main(String[] args) {
		SpringApplication.run(ProyectoMomentumApplication.class, args);
	}
}
