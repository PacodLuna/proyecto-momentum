package com.lfds.javaweb.proyecto.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lfds.javaweb.proyecto.model.Check;
import com.lfds.javaweb.proyecto.model.Register;
import com.lfds.javaweb.proyecto.service.ErrorService;
import com.lfds.javaweb.proyecto.service.RegisterService;

@RestController
@RequestMapping("/api/v1/")
public class RegisterController {

	@Autowired
	RegisterService registerService;

	//1st Endpoint
	@RequestMapping(value = "users/{is}/{mes}", method = RequestMethod.GET)
	public float hoursPerMonthAndName(@PathVariable String is, @PathVariable String mes) {
		if (registerService.getByMonthAndName(is, mes) == -1.0)
			throw new ErrorService("Está vacío, ves");
		return registerService.getByMonthAndName(is, mes);
	}

	//2nd Endpoint
	@RequestMapping(value = "users", method = RequestMethod.POST)
	public List<Register> registersPerPeriodAndName(@RequestParam String is, @RequestParam String startDate,
			@RequestParam String endDate) {
		if (registerService.getByPeriodAndName(is, startDate, endDate).isEmpty())
			throw new ErrorService("Está vacío, ves");
		return registerService.getByPeriodAndName(is, startDate, endDate);
	}

	//3rd Endpoint
	@RequestMapping(value = "period/{mes}", method = RequestMethod.GET)
	public List<Register> registerPerMonth(@PathVariable String mes) {
		if (registerService.getByMonth(mes).isEmpty())
			throw new ErrorService("Está vacío, ves");
		return registerService.getByMonth(mes);
	}

	//4rth Endpoint
	@RequestMapping(value = "period", method = RequestMethod.POST)
	public List<Register> allRegisterPerPeriod(@RequestParam String startDate, @RequestParam String endDate) {
		if (registerService.getAllByPeriod(startDate, endDate).isEmpty())
			throw new ErrorService("Está vacío, ves");
		return registerService.getAllByPeriod(startDate, endDate);
	}

	@RequestMapping(value = "init", method = RequestMethod.GET)
	public void init() {
		registerService.uploadDatabase();
	}
}
