package com.lfds.javaweb.proyecto.service;

public class ErrorService extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrorService(String message) {
		super(message);
	}
	
}
