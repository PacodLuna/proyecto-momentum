package com.lfds.javaweb.proyecto.service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lfds.javaweb.proyecto.model.Check;
import com.lfds.javaweb.proyecto.model.Register;
import com.lfds.javaweb.proyecto.repositories.RegisterRepository;

@Service
public class RegisterServiceImpl implements RegisterService {

	@Autowired
	private RegisterRepository registerRepository;

	@Override
	public void save(Register r) {
		registerRepository.save(r);
	}

	@Override
	public float getByMonthAndName(String name, String month) {
		Date date, date1;
		Long hours = 0l;
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		List<Register> list = registerRepository.getByMonthAndName(name, month).stream()
				.filter(r -> r.getDate().split("-")[1].equals(month)).collect(Collectors.toList());
		if (!list.isEmpty()) {
			try {
				for (Register reg : list) {
					date = format.parse(reg.getEntrada());
					date1 = format.parse(reg.getSalida());
					hours += (date1.getTime() - date.getTime());
				}
				return hours.floatValue() / 3600000;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return -1.0f;
		}
		return -1.0f;
	}

	@Override
	public List<Register> getByMonth(String month) {
		return registerRepository.getByMonth(month).stream().filter(r -> r.getDate().split("-")[1].equals(month))
				.collect(Collectors.toList());
	}

	@Override
	public List<Register> getByPeriodAndName(String is, String startDate, String endDate) {
		LocalDate date, date1;
		date = LocalDate.parse(startDate).minusDays(1);
		date1 = LocalDate.parse(endDate).plusDays(1);
		return registerRepository.getByPeriodAndName(is).stream()
				.filter(d -> LocalDate.parse(d.getDate()).isAfter(date))
				.filter(d -> LocalDate.parse(d.getDate()).isBefore(date1)).collect(Collectors.toList());
	}

	@Override
	public List<Register> getAllByPeriod(String startDate, String endDate) {
		LocalDate date, date1;
		date = LocalDate.parse(startDate).minusDays(1);
		date1 = LocalDate.parse(endDate).plusDays(1);
		return ((List<Register>) registerRepository.findAll()).stream()
				.filter(d -> LocalDate.parse(d.getDate()).isAfter(date))
				.filter(d -> LocalDate.parse(d.getDate()).isBefore(date1)).collect(Collectors.toList());
	}

	@Override
	public List<Register> getAll() {
		return (List<Register>) registerRepository.findAll();
	}

	@Override
	public boolean uploadDatabase() {
		final String MOMENTUM_FILE_PATH = "src\\main\\resources\\Registros_Momentum.xls";
		final int R_IN_OUT = 4;
		final int R_NAME = 2;
		final int R_ID = 3;
		int i = 1, j = 1;
		boolean bool = true;
		if (this.getAll().isEmpty()) {
			Check entrada = null, salida = null, siguiente = null;
			List<Check> check_list = new ArrayList<>();
			try {
				Workbook workbook = WorkbookFactory.create(new File(MOMENTUM_FILE_PATH));
				Sheet sheet = workbook.getSheetAt(0);
				DataFormatter dataFormatter = new DataFormatter();
				sheet.forEach(row -> {
					String s_date = dataFormatter.formatCellValue(row.getCell(R_IN_OUT));
					if (!s_date.equals("Date/Time")) {
						check_list.add(new Check(dataFormatter.formatCellValue(row.getCell(R_NAME)),
								dataFormatter.formatCellValue(row.getCell(R_IN_OUT)),
								dataFormatter.formatCellValue(row.getCell(R_ID))));
					}
				});
				workbook.close();
			} catch (InvalidFormatException | IOException e) {
				e.printStackTrace();
				bool = false;
			}
			entrada = check_list.get(0);
			salida = check_list.get(0);
			while (i < check_list.size() - 1) {
				if (entrada.getDate().equals(check_list.get(i).getDate())) {
					if (entrada.getName().equals(check_list.get(i).getName())) {
						salida = check_list.get(i);
						siguiente = check_list.get(i + 1);
						if (!siguiente.getDate().equals(salida.getDate())) {
							this.save(new Register(j, entrada.getName(), entrada.getTime(), salida.getTime(),
									entrada.getDate(), entrada.getId()));
							j++;
						}
					}
				} else {
					entrada = check_list.get(i);
					if (i == check_list.size() - 2) {
						siguiente = check_list.get(i + 1);
						if (entrada.getDate().equals(siguiente.getDate())) {
							this.save(new Register(j, entrada.getName(), entrada.getTime(), siguiente.getTime(),
									entrada.getDate(), entrada.getId()));
							j++;
						}
					}
				}
				i++;
			}
			System.out.println("Base de datos actualizada");
		}
		return bool;
	}
}
