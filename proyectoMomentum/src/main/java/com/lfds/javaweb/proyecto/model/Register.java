package com.lfds.javaweb.proyecto.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Register {
	@Id
	private int id_bd;
	private String name;
	private String entrada;
	private String salida;
	private String date;
	private String id;

	public Register() { }

	public Register(int id_bd, String name, String entrada, String salida, String date, String id) {
		super();
		this.id_bd = id_bd;
		this.name = name;
		this.entrada = entrada;
		this.salida = salida;
		this.date = date;
		this.id = id;
	}

	public int getId_bd() {
		return id_bd;
	}

	public void setId_bd(int id_bd) {
		this.id_bd = id_bd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEntrada() {
		return entrada;
	}

	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}

	public String getSalida() {
		return salida;
	}

	public void setSalida(String salida) {
		this.salida = salida;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Register [id_bd=" + id_bd + ", name=" + name + ", entrada=" + entrada + ", salida=" + salida + ", date="
				+ date + ", id=" + id + "]";
	}

	
	
}
