package com.lfds.javaweb.proyecto.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lfds.javaweb.proyecto.model.Errorcito;

@Controller
public class ErrorcitoController implements ErrorController{

	@Override
	public String getErrorPath() {
		return "/error";
	}
	
	@RequestMapping(value = "/error")
	@ResponseBody
	public Errorcito handleError(HttpServletRequest request){
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
        return new Errorcito(exception.getCause().getMessage(), statusCode.intValue());
	}

}
