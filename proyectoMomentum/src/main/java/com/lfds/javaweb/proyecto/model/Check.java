package com.lfds.javaweb.proyecto.model;

public class Check {
	String name;
	String date;
	String time;
	String id;
	
	public Check() { }

	public Check(String name, String date, String id) {
		super();
		this.name = name;
		String[] dateTime = date.split(" ");
		this.date = dateTime[0];
		this.time = dateTime[1];
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Check [name=" + name + ", date=" + date + ", time=" + time + ", id=" + id + "]";
	}
}
