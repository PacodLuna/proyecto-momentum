<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="1">
<tr>
    <th>IS</th>
    <th>Date</th>
    <th>Hour In</th>
    <th>Hour Out</th>
</tr>
<tr>
<c:forEach items="${list}" var="list">
 <td>${list.name}</td>
  <td>${list.date}</td> 
  <td>${list.entrada}</td>
   <td>${list.salida}</td>
  
 </tr>             
</c:forEach>
</table>
<a href="/ProyectoMomentum/index">Return to index</a>
</body>
</html>