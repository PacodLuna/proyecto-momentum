# Proyecto Momentum

Proyecto final momentum 2019

**Instrucciones:**
El script *script.sql* debe ser ejecutado antes de compilar el programa, debido a que se necesita la existencia de la base de datos y la estructura de las tablas para funcionar.
En este archivo unicamente se verifica que exista el schema, de no existir se crea, la estructura de la tabla de registros y finalmente la estructura y contenido de la tabla de users (para el wsdl de login).

# API
1.  Primer endpoint: http://localhost:8080/ProyectoMomentum/users/{is}/{mes}
    En este se debe especificar un usuario por is y el mes que se quiere consultar. Este calcula el total de horas trabajadas por un trabajador en el mes especificado.
    ** Importante**: El mes debe estar en formato númerico con 0 al principio en caso de ser de un solo dígito
    Ejemplo: http://localhost:8080/ProyectoMomentum/api/v1/users/CXMG/02
2.  Segundo endpoint: http://localhost:8080/ProyectoMomentum/users
    Por medio de un método post muestra todos los registros de un usuario en un periodo específico.
    Se tienen que especificar 3 atributos en el método:
    1.  is: el is del usuario
    2.  startDate: la fecha de inicio
    3.  endDate: la fecha de término
     ** Importante** : la fecha debe de tener formato yyyy-mm-dd
    Ejemplo:
    http://localhost:8080/ProyectoMomentum/api/v1/users?is=CXMG&startDate=2019-02-01&endDate=2019-02-28
3.  Tercer endpoint: http://localhost:8080/ProyectoMomentum/period/{mes}
    Se especifica el mes en el cual se quiere hacer la consulta y se muestran todos los registros de todos los usuarios registrados.
     ** Importante** : El mes debe estar en formato númerico con 0 al principio en caso de ser de un solo dígito
    Ejemplo:
    http://localhost:8080/ProyectoMomentum/api/v1/period/01
4.  Cuarto endpoint: http://localhost:8080/ProyectoMomentum/period
    Se especifica el periodo en el cual se quiere hacer la consulta y se muestran todos los registros de todos los usuarios registrados.
    Se tienen que especificar 2 atributos en el método:
    1.  startDate: la fecha de inicio
    2.  endDate: la fecha de término
    Ejemplo:
    http://localhost:8080/ProyectoMomentum/api/v1/period?startDate=2019-02-01&endDate=2019-02-28

# Vistas
Todas las vistas son accesables desde la página principal http://localhost:8080/ProyectoMomentum/ o http://localhost:8080/ProyectoMomentum/index, en la cual se pueden encontrar los links a todas ellas.
Las instrucciones para el uso correcto de los formularios son exactamente las mismas que los endpoints:
*  Las fecha deben de tener formato yyyy-mm-dd
*  El mes debe estar en formato númerico con 0 al principio en caso de ser de un solo dígito

# WSDL
Este wsdl simplemente regresa un true si los datos ingresados concuerdan con los datos en la tabla de users dentro de la base de datos, en este caso son:
*  username: CXMG
*  passwd: 1234

# Información importante:
La base de datos se llena mediante un test típicamente, que verifica que el excel archivo de registros exista y que se pueda acceder a la tabla, en caso de que la tabla ya tenga registros este método no se ejecuta.
En caso de que se haga truncate a la tabla o simplemente se hayan eliminado los registros se puede llenar de dos maneras.
1.  Con el uso del endpoint http://localhost:8080/ProyectoMomentum/api/v1/init
2.  Accediendo a la página principal del proyecto http://localhost:8080/ProyectoMomentum/ o http://localhost:8080/ProyectoMomentum/index