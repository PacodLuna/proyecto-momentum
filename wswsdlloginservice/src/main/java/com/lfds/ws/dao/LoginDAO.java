package com.lfds.ws.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.lfds.ws.beans.User;
import com.lfds.ws.service.LoginService;

public class LoginDAO {

	public static boolean login(User usr) {
		ResultSet rs = LoginService.login(usr);
		String pw;
		try {
			while(rs.next()) {
				pw = rs.getString("passwd");
				if(pw.equals(usr.getPasswd())) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
