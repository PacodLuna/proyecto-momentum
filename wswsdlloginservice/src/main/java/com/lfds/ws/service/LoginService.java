package com.lfds.ws.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.lfds.ws.beans.User;

public class LoginService {

	public static ResultSet login(User usr) {
		String query = "SELECT * FROM users WHERE (username = ?);";
		System.out.println(usr.getName());
		ResultSet rs = null;
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, usr.getName());
			rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
}
