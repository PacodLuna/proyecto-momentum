package com.lfds.ws.soap;

import org.apache.cxf.feature.Features;

import com.jaom.ws.trainings.CustomerOrdersPortType;
import com.jaom.ws.trainings.LoginOrderRequest;
import com.jaom.ws.trainings.LoginOrderResponse;
import com.lfds.ws.beans.User;
import com.lfds.ws.dao.LoginDAO;


@Features(features = "org.apache.cxf.feature.LoggingFeature")
public class CustomerOrdersWSImpl implements CustomerOrdersPortType {
	
	@Override
	public LoginOrderResponse loginOrder(LoginOrderRequest request) {
		String username = request.getUserId();
		String passwd = request.getPasswd();
		User usr = new User(username, passwd);
		boolean loginResult = LoginDAO.login(usr);
		LoginOrderResponse response = new LoginOrderResponse();
		response.setResult(loginResult);
		return response;
	}

}
